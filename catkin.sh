#!/bin/bash

BASEDIR=$(dirname "$0")		
unbuffer catkin "$@" | tee /tmp/catkin_output.txt
if grep -q "Failed:    None." /tmp/catkin_output.txt; then
    mpg123 -q $BASEDIR/success.mp3 
else
    mpg123 -q $BASEDIR/failed.mp3 
fi
